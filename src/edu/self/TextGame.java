package edu.self;

import java.util.Scanner;

// The assignment can be found here:
// https://gist.github.com/noidontdig/92a4ead9fe8f2f21f178

public class TextGame {

    public static void main(String[] args) {

        // Create a new Scanner object listening for input
        Scanner input = new Scanner(System.in);
        // Set scanner's delimiter to newline
        input.useDelimiter("\n");

        // Begin with an initial prompt for the user
        // Pass the input object so we can reuse it
        initialPrompt(input);

    }

    public static void initialPrompt(Scanner input) {

        // Prompt user and listen for input
        System.out.println("You're on the way out of the house, and suddenly you realize you " +
                "don't have your keys! Do you want to leave any way? (Y or N)");
        String response = input.next();

        // Check for lowercase or uppercase Y
        if (response.equals("Y") || response.equals("y")) {
            // Print GAME OVER and don't ask next question
            System.out.println("You leave your house and get locked out. GAME OVER");
        }
        // Check for lowercase or uppercase N
        else if (response.equals("N") || response.equals("n")) {
            // give user extra context
            System.out.println("You stay to look for your keys. Your house has two rooms, " +
                    "and you're in the first room. There are no keys here.");
            // Begin next prompt
            roomQuestion(input);
        } else {
            // If the response is unexpected, print error message and repeat initial prompt
            System.out.println("I couldn't understand your response. Let's try this again.");
            initialPrompt(input);
        }
    }

    public static void roomQuestion(Scanner input) {

        // Prompt user and listen for input
        System.out.println("Do you want to look in the other room? (Y or N)");
        String response = input.next();

        // Check for lowercase or uppercase Y
        if (response.equals("Y") || response.equals("y")) {
            // Print GAME OVER and don't ask next question
            System.out.println("No keys here!");
            roomQuestion(input);
        }
        // Check for lowercase or uppercase N
        else if (response.equals("N") || response.equals("n")) {
            // Begin next prompt and question
            System.out.println("A sudden thought! Maybe your keys are in your pocket... Want to check? (Y or N)");
            pocketQuestion(input);
        } else {
            // If the response is unexpected, print error message and repeat the question
            System.out.println("I couldn't understand your response. Let's try this again.");
            roomQuestion(input);
        }
    }

    public static void pocketQuestion(Scanner input) {

        // Listen for input
        String response = input.next();

        // Check for lowercase or uppercase Y
        if (response.equals("Y") || response.equals("y")) {
            // Tell user they won
            System.out.println("You found them! The keys were in your pocket all along! YOU WIN!!!");
        }
        // Check for lowercase or uppercase N
        else if (response.equals("N") || response.equals("n")) {
            // Print prompt and begin question again
            System.out.println("But maybe you should... Want to check your pocket? (Y or N)");
            pocketQuestion(input);
        } else {
            // If the response is unexpected, print error message and repeat the question
            System.out.println("I couldn't understand your response. Let's try this again.");
            pocketQuestion(input);
        }
    }
}